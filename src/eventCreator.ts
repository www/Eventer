import * as fs from "fs";
import { trainCase } from "case-anything";
import { EventDetails } from "./EventDetails";
import moment from "moment";
import "dotenv/config";

import {
  cloneRepo,
  commitRepo,
  pushRepo,
  createAndPublishNewBranch,
} from "./gitClient";
import { makePullRequest } from "./giteaClient";

const DATE_FORMAT = "MMMM DD yyyy HH:mm";

function getSchoolSeason() {
  const month = new Date().getMonth();
  if (month <= 4) {
    return "winter";
  } else if (month <= 8) {
    return "spring";
  } else {
    return "fall";
  }
}

function createMarkdownFile(
  eventDetails: EventDetails,
  parentDirectory: string,
  posterLink?: string
): boolean | string {
  const {
    name,
    longDescription,
    shortDescription,
    startDate,
    endDate,
    location,
    online,
    registerLink,
  } = eventDetails;

  const formatDate = (date: Date) => moment(date).format(DATE_FORMAT);

  const fileName = trainCase(eventDetails.name);
  const folder = `${parentDirectory}/content/events/${new Date().getFullYear()}/${getSchoolSeason()}`;
  const path = `${folder}/${fileName}.md`;
  const endDateCleaned = `${
    endDate ? `endDate: '${formatDate(endDate)}'\n` : ""
  }`;
  const locationCleaned = `${
    location && location.length ? `location: '${location}'\n` : ""
  }`;
  const posterLinkCleaned = `${posterLink ? `poster: '${posterLink}'\n` : ""}`;
  const registerLinkCleaned = `${
    registerLink ? `registerLink: '${registerLink}'\n` : ""
  }`;

  const md =
    `---\nname: '${name}'\n` +
    `short: '${shortDescription}'\n` +
    `startDate: '${formatDate(startDate)}'\n` +
    `${endDateCleaned}` +
    `online: ${(online ?? false).toString()}\n` +
    `${locationCleaned}` +
    `${posterLinkCleaned}` +
    `${registerLinkCleaned}---\n\n` +
    `${longDescription}\n`;

  if (!fs.existsSync(folder)) {
    fs.mkdirSync(folder, { recursive: true });
  }
  // create file and write md to it
  fs.writeFileSync(path, md);

  return true;
}

function copyPoster(posterLink: string, repoPath: string): string {
  const fileName = posterLink.split("/").at(-1);
  const relativePosterDirectory = `images/events/${new Date().getFullYear()}/${getSchoolSeason()}`
  const relativePosterDest = `${relativePosterDirectory}/${fileName}`
  const absolutePosterDirectory = `${repoPath}/${relativePosterDirectory}`;
  if (!fs.existsSync(absolutePosterDirectory)) {
    fs.mkdirSync(absolutePosterDirectory, { recursive: true });
  }
  const absolutePosterDest = `${absolutePosterDirectory}/${fileName}`;
  fs.copyFileSync(posterLink, absolutePosterDest);

  return relativePosterDest;
}

async function completelyCreateEvent(
  eventDetails: EventDetails,
  prTitle: string,
  prBody: string,
  posterLink?: string
): Promise<string> {
  const owner = process.env.PR_OWNER;
  const repoName = process.env.PR_REPONAME;
  const httpAddr = `git@csclub.uwaterloo.ca:${owner}/${repoName}.git`;
  const tmpFolderPath = await cloneRepo(httpAddr);
  const branchName = await createAndPublishNewBranch(tmpFolderPath, prTitle);
  const repoPosterLink = copyPoster(posterLink, tmpFolderPath);
  const mdFile = createMarkdownFile(
    eventDetails,
    tmpFolderPath,
    repoPosterLink
  );
  if (mdFile) {
    await commitRepo(tmpFolderPath);
    await pushRepo(tmpFolderPath);
  } else {
    throw new Error("Failed to create markdown file");
  }

  const stagingLink = "https://csclub.uwaterloo.ca/~a3thakra/csc/" + branchName;

  // pull request
  const reviewers_array = process.env.PR_REVIEWERS.split(",");
  const reviewers = reviewers_array.map((reviewer) => reviewer.trim());

  const pullUrl = await makePullRequest(
    owner,
    repoName,
    prTitle,
    prBody + `\n\n ${stagingLink}`,
    branchName,
    reviewers
  );
  return pullUrl;
}

export { createMarkdownFile, completelyCreateEvent };
