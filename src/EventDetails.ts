interface EventDetails {
  name: string;
  shortDescription: string;
  longDescription: string;
  startDate: Date;
  endDate?: Date;
  registerLink?: string;
  location?: string;
  online?: boolean;
}

export { EventDetails };
