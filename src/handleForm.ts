import { createEmitAndSemanticDiagnosticsBuilderProgram } from "typescript";
import { EventDetails } from "./EventDetails";

function validateForm(
  name: string,
  shortDescription: string,
  longDescription: string,
  start: string,
  end: string,
  registerLink: string,
  location: string,
  online?: string,
  file?: Express.Multer.File
): EventDetails | string {
  if (!name) {
    return "Event name is required.";
  }

  if (!shortDescription) {
    return "Short description is required.";
  }

  if (!longDescription) {
    return "Long description is required.";
  }

  if (!file) {
    return "Poster is required.";
  }

  if (start && end) {
    const startTime = new Date(start);
    const endTime = new Date(end);

    const checkDateIsValid = (date: Date) =>
      date instanceof Date && !isNaN(date.valueOf());

    if (
      !checkDateIsValid(startTime) ||
      !checkDateIsValid(endTime) ||
      startTime > endTime
    ) {
      return "End time must be greater than or equal to start time.";
    }
  }

  return {
    name: name,
    shortDescription: shortDescription,
    longDescription: longDescription,
    startDate: new Date(start),
    endDate: end ? new Date(end) : undefined,
    registerLink: registerLink,
    location: location,
    online: online === "on" ? true : false, // The form sends "on" if the checkbox is checked
  };
}

export { validateForm };
