window.addEventListener("load", () => {
    function sendData() {
        const XHR = new XMLHttpRequest();
        const FD = new FormData(form);

        XHR.addEventListener("load", (event) => {
					if(event.target.status == 200){
						window.location.href = "./success.html";
					}else{
						alert("Something went wrong: " + event.target.responseText)
					}
        });

        XHR.addEventListener("error", (event) => {
            alert("Oops! Something went wrong.");
        });

        // Set up request
        XHR.open("POST", "./event");

        // The data sent is what the user provided in the form
        XHR.send(FD);
    }

    const form = document.getElementById("eventForm");

    form.addEventListener("submit", (event) => {
        event.preventDefault();
        if (!confirm("You are about to submit the form. Continue? Note that it may take upto 5 minutes to finish submitting!")) {
            return;
        }

        // Form validation
        const name = document.getElementById("name").value;
        const short = document.getElementById("short").value;
        const long = document.getElementById("long").value;
        const startDate = document.getElementById("start").value;
        const endDate = document.getElementById("end").value;
        const startDateTime = new Date(startDate).getTime();
        const endDateTime = new Date(endDate).getTime();

        // Check if required fields are filled in
        if (name === "" || short === "" || long === "" || startDate === "") {
            alert("Please fill in all required fields.");
            return;
        }

        // Check if date and time inputs are valid
        if (isNaN(startDateTime) || (endDate !== "" && isNaN(endDateTime))) {
            alert("Please enter a valid date and time.");
            return;
        }

        // Check if end date and time are after start date and time
        if (endDateTime < startDateTime) {
            alert("The end date and time must be after the start date and time.");
            return;
        }

        sendData();
    });
});